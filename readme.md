Набор утилит DocMaker
=====================

Используется для генерации документации по ЕСПД средствами DocBook.



Содержимое репозитория
----------------------

* _example - пример документации
* DocBook  - стили DocBook
* styles   - стили формирования документации (ЕСПД и пр.)
* fonts    - рекомендуемые шрифты
* fop      - fo-процессор для содания pdf, png, rtf и пр.
* gost     - ГОСТ-ы ЕСПД pdf
* jre      - java portable
* xsltproc - xslt-процессор



Как использовать?
-----------------

Рекомендуется строить документацию на базе примера _example.
Там показана структура многофайлового документа, собираемого скриптом MakePdf.cmd.

```
MakePdf.cmd Path/to/DocMaker
```
