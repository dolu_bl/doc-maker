<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.1"
    xmlns:d="http://docbook.org/ns/docbook" 
    exclude-result-prefixes="d" >

    <xsl:import href="espd.xsl"/>

    <xsl:param name="body.font.family"      >Liberation Sans</xsl:param>
    <xsl:param name="sans.font.family"      >Liberation Sans</xsl:param>
    <xsl:param name="title.font.family"     >Liberation Sans</xsl:param>
    <xsl:param name="symbol.font.family"    >Symbol</xsl:param>
    <xsl:param name="monospace.font.family" >Liberation Mono</xsl:param>
    <xsl:param name="body.font.master"      >10</xsl:param>
    <xsl:param name="espd.text-indent"      >8mm</xsl:param>
    <xsl:param name="double.sided"          >0</xsl:param>
    <xsl:param name="fop1.extensions"       >1</xsl:param>

    <xsl:param name="espd.decimal" >ЕИАУ.360049.001-56-01</xsl:param>

    <!-- Добавить для ВЫКЛЮЧЕНИЯ ЛУ:
    <xsl:template name="book.titlepage.recto"/>
    <xsl:template name="book.titlepage.before.recto"/>
    -->

</xsl:stylesheet>
