@echo off

if "%1"=="" goto HowTo



set DocMaker=%1
set ExampleBook=%~dp0
set CurrentPath=%CD%

cd %~dp0


mkdir build


set PATH=%DocMaker%\jre\bin\;%PATH%


echo Combining data...
%DocMaker%\xsltproc\xmllint.exe          ^
    --xinclude                           ^
    --noent                              ^
    --output %ExampleBook%build\book.xml ^
    %ExampleBook%texts\book.xml
echo.


echo Styling data...
%DocMaker%\xsltproc\xsltproc.exe        ^
    --path %DocMaker%\styles\espd       ^
    %ExampleBook%stylesheets\sample.xsl ^
    %ExampleBook%build\book.xml         > %ExampleBook%build\result.fo
echo.


echo Building book...
%DocMaker%\fop\fop-2.0\fop         ^
 -c   %DocMaker%\fop\fop.xml       ^
 -fo  %ExampleBook%build\result.fo ^
 -pdf %ExampleBook%build\result.pdf
echo.


cd %CurrentPath%

Exit /B



:HowTo
echo USAGE
echo     MakePdf.cmd Path/to/DocMaker
Exit /B
